# go.pluginmanager

> 一个基于小型api约定的golang插件加载库

## Use:

1. 编写插件Api
    ```golang
    // plugin-api.go
    const (
        ModuleDefaltType = "default"
    )

    type Module struct {
        Name    string
        Handler func() string
    }
    ```

2. 编写插件实现
    ```golang
    // plugin-hello.go
    func sayHello() string {
        fmt.Println("Hello, I'm is plugin-hello")
        return "已调用 sayHello"
    }

    var PluginMeta = manager.NewPluginMeta("hello", "v0.1",
        api.ModuleDefaltType, api.Module{
            Name:    "plugin-hello",
            Handler: sayHello,
        })

    // api.ModuleDefaltType 为插件类型
    // api.Module   为插件功能接口约定实现

    ```

3. 编写插件加载器实现
    ```golang
    // plugin-loader.go
    func main() {
        var pm = manager.NewPluginManager(".", ".so")
        plugins, _ := pm.LoadPlugins()

        for _, plugin := range plugins {
            fmt.Println("发现插件:", plugin.PluginName())
            fmt.Println("名称:", plugin.Plugin().(api.Module).Name)
            fmt.Println("调用:", plugin.Plugin().(api.Module).Handler())
        }
    }
    ```

## 示例

1. 构建示例
    ```bash
    $ ./build-plugin-loader.sh 
    $ ./plugin-loader 
    >>> 加载插件: hello
    发现插件: hello
    名称: plugin-hello
    Hello, I'm is plugin-hello
    调用: 已调用 sayHello
    ```