package main

import (
	"fmt"

	"gitee.com/zinface/go.pluginmanager/examples/api"
	manager "gitee.com/zinface/go.pluginmanager/pkg"
)

func sayHello() string {
	fmt.Println("Hello, I'm is plugin-hello")
	return "已调用 sayHello"
}

var PluginMeta = manager.NewPluginMeta("hello", "v0.1",
	api.ModuleDefaltType, api.Module{
		Name:    "plugin-hello",
		Handler: sayHello,
	})
