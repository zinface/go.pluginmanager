package main

import (
	"fmt"

	"gitee.com/zinface/go.pluginmanager/examples/api"
	manager "gitee.com/zinface/go.pluginmanager/pkg"
)

func main() {
	var pm = manager.NewPluginManager(".", ".so")
	plugins, _ := pm.LoadPlugins()

	for _, plugin := range plugins {
		fmt.Println("发现插件:", plugin.PluginName())
		fmt.Println("名称:", plugin.Plugin().(api.Module).Name)
		fmt.Println("调用:", plugin.Plugin().(api.Module).Handler())
	}
}
