package api

const (
	ModuleDefaltType = "default"
)

type Module struct {
	Name    string
	Handler func() string
}
