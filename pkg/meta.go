package manager

type IPluginManagerMeta interface {
	PluginName() string
	PluginVersion() string
	PluginMetaType() string
	Plugin() interface{}
}

type PluginMeta struct {
	Name     string
	Version  string
	MetaType string
	MetaData interface{}
}

func NewPluginMeta(name string, version string, meta_name string, plugin interface{}) PluginMeta {
	return PluginMeta{
		Name:     name,
		Version:  version,
		MetaType: meta_name,
		MetaData: plugin,
	}
}

func (pm PluginMeta) PluginName() string {
	return pm.Name
}
func (pm PluginMeta) PluginVersion() string {
	return pm.Version
}
func (pm PluginMeta) PluginMetaType() string {
	return pm.MetaType
}
func (pm PluginMeta) Plugin() interface{} {
	return pm.MetaData
}
